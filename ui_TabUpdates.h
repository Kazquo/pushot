/********************************************************************************
** Form generated from reading UI file 'TabUpdates.ui'
**
** Created by: Qt User Interface Compiler version 4.8.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TABUPDATES_H
#define UI_TABUPDATES_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TabUpdates
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *lblStatus;
    QPushButton *btnCheckUpdates;

    void setupUi(QWidget *TabUpdates)
    {
        if (TabUpdates->objectName().isEmpty())
            TabUpdates->setObjectName(QString::fromUtf8("TabUpdates"));
        TabUpdates->resize(400, 71);
        verticalLayout = new QVBoxLayout(TabUpdates);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        lblStatus = new QLabel(TabUpdates);
        lblStatus->setObjectName(QString::fromUtf8("lblStatus"));
        lblStatus->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(lblStatus);

        btnCheckUpdates = new QPushButton(TabUpdates);
        btnCheckUpdates->setObjectName(QString::fromUtf8("btnCheckUpdates"));

        verticalLayout->addWidget(btnCheckUpdates);


        retranslateUi(TabUpdates);

        QMetaObject::connectSlotsByName(TabUpdates);
    } // setupUi

    void retranslateUi(QWidget *TabUpdates)
    {
        TabUpdates->setWindowTitle(QApplication::translate("TabUpdates", "Form", 0, QApplication::UnicodeUTF8));
        lblStatus->setText(QApplication::translate("TabUpdates", "No updates found. (last checked : xxx)", 0, QApplication::UnicodeUTF8));
        btnCheckUpdates->setText(QApplication::translate("TabUpdates", "Check for updates", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class TabUpdates: public Ui_TabUpdates {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TABUPDATES_H
