/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QToolBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionGeneral;
    QAction *actionCredentials;
    QAction *actionUpdates;
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QToolBar *toolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(400, 284);
        actionGeneral = new QAction(MainWindow);
        actionGeneral->setObjectName(QString::fromUtf8("actionGeneral"));
        actionGeneral->setCheckable(true);
        actionGeneral->setChecked(true);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/tabs/resources/General.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionGeneral->setIcon(icon);
        actionCredentials = new QAction(MainWindow);
        actionCredentials->setObjectName(QString::fromUtf8("actionCredentials"));
        actionCredentials->setCheckable(true);
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/tabs/resources/Credentials.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionCredentials->setIcon(icon1);
        actionUpdates = new QAction(MainWindow);
        actionUpdates->setObjectName(QString::fromUtf8("actionUpdates"));
        actionUpdates->setCheckable(true);
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/tabs/resources/Updates.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionUpdates->setIcon(icon2);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        MainWindow->setCentralWidget(centralwidget);
        toolBar = new QToolBar(MainWindow);
        toolBar->setObjectName(QString::fromUtf8("toolBar"));
        toolBar->setMovable(false);
        toolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        toolBar->setFloatable(false);
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar);

        toolBar->addAction(actionGeneral);
        toolBar->addAction(actionCredentials);
        toolBar->addAction(actionUpdates);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "PuShot", 0, QApplication::UnicodeUTF8));
        actionGeneral->setText(QApplication::translate("MainWindow", "General", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionGeneral->setToolTip(QApplication::translate("MainWindow", "General Settings", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionCredentials->setText(QApplication::translate("MainWindow", "Credentials", 0, QApplication::UnicodeUTF8));
        actionUpdates->setText(QApplication::translate("MainWindow", "Updates", 0, QApplication::UnicodeUTF8));
        toolBar->setWindowTitle(QApplication::translate("MainWindow", "toolBar", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
