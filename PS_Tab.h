/********************************************************************************
 *																				*
 *		PS_Tab.cpp - Created 25/08/2014 10:00									*
 *																				*
 *		By Tuetuopay - http://tuetuopay.fr										*
 *																				*
 *		This file is part of PuShot.											*
 *																				*
 *		PuShot is free software: you can redistribute it and/or modify			*
 *		it under the terms of the GNU General Public License as published by	*
 *		the Free Software Foundation, either version 3 of the License, or		*
 *		(at your option) any later version.										*
 *																				*
 *		PuShot is distributed in the hope that it will be useful,				*
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *		GNU General Public License for more details.							*
 *																				*
 *		You should have received a copy of the GNU General Public License		*
 *		along with PuShot.  If not, see <http://www.gnu.org/licenses/>.			*
 *																				*
 ********************************************************************************/

#ifndef _PS_TAB_H
#define _PS_TAB_H
#include <QtWidgets>
#include <QWidget>

class PS_Tab : public QWidget	{
	Q_OBJECT
public:
	PS_Tab (QWidget *parent = NULL) : QWidget (parent) {
		saveSize ();
	}
	
	inline int initWidth () { return _width; }
	inline int initHeight () { return _height; }
	
	inline void saveSize () {
		_height = height ();
		_width = width ();
	}
	
protected:
	int _width, _height;
};

#endif /* defined (_PS_TAB_H) */
