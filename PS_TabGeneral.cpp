/********************************************************************************
 *																				*
 *		PS_TabGeneral.cpp - Created 25/08/2014 01:19							*
 *																				*
 *		By Tuetuopay - http://tuetuopay.fr										*
 *																				*
 *		This file is part of PuShot.											*
 *																				*
 *		PuShot is free software: you can redistribute it and/or modify			*
 *		it under the terms of the GNU General Public License as published by	*
 *		the Free Software Foundation, either version 3 of the License, or		*
 *		(at your option) any later version.										*
 *																				*
 *		PuShot is distributed in the hope that it will be useful,				*
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *		GNU General Public License for more details.							*
 *																				*
 *		You should have received a copy of the GNU General Public License		*
 *		along with PuShot.  If not, see <http://www.gnu.org/licenses/>.			*
 *																				*
 ********************************************************************************/

#include "PS_TabGeneral.h"
#include "PS_Utils.h"

PS_TabGeneral::PS_TabGeneral (QWidget *parent) : PS_Tab (parent)	{
	setupUi (this);
	saveSize ();
	
	leScreenshotPath->setText (PS_Utils::getDesktopLocation ());
	
	// QObject::connect (btnBrowse, SIGNAL(clicked()), this, SLOT(on_btnBrowse_clicked()));
}

void PS_TabGeneral::on_btnBrowse_clicked ()	{
	QString path = QFileDialog::getExistingDirectory (parentWidget (), tr ("Screenshots directory"), leScreenshotPath->text ());
	if (path != "")	{
		leScreenshotPath->setText (path);
		emit screenshotsPathChanged (path);
	}
}

void PS_TabGeneral::saveSettings (QSettings &s)	{
	s.setValue ("startOnBoot", cbStartOnBoot->isChecked ());
	s.setValue ("notifVisual", cbNotifVisual->isChecked ());
	s.setValue ("notifSound", cbNotifSound->isChecked ());
	s.setValue ("copyToClipboard", cbCopyClipboard->isChecked ());
	s.setValue ("openInBrowser", cbOpenInBrowser->isChecked ());
	s.setValue ("deleteFile", cbDelete->isChecked ());
	s.setValue ("systemKeys", cbSystemKeys->isChecked ());
	
	s.setValue ("screenshotPath", screenshotsPath ());
}
void PS_TabGeneral::loadSettings (QSettings &s)	{
	cbStartOnBoot->setChecked (s.value ("startOnBoot", true).toBool ());
	cbNotifVisual->setChecked (s.value ("notifVisual", true).toBool ());
	cbNotifSound->setChecked (s.value ("notifSound", true).toBool ());
	cbCopyClipboard->setChecked (s.value ("copyToClipboard", true).toBool ());
	cbOpenInBrowser->setChecked (s.value ("openInBrowser", false).toBool ());
	cbDelete->setChecked (s.value ("deleteFile", false).toBool ());
	cbSystemKeys->setChecked (s.value ("systemKeys", true).toBool ());
	
	leScreenshotPath->setText (s.value("screenshotPath", PS_Utils::getDesktopLocation ()).toString ());
}
void PS_TabGeneral::on_leScreenshotPath_textChanged (QString path)	{
	emit screenshotsPathChanged (path);
}

bool PS_TabGeneral::notifSound () const	{
	return cbNotifSound->isChecked ();
}
bool PS_TabGeneral::notifVisual () const	{
	return cbNotifVisual->isChecked ();
}
bool PS_TabGeneral::openInBrowser () const	{
	return cbOpenInBrowser->isChecked ();
}
bool PS_TabGeneral::deleteFile () const	{
	return cbDelete->isChecked ();
}

PS_TabGeneral::~PS_TabGeneral () {}
