<?php

/********************************************************************************
 *																				*
 *		PS_API.php - Created 25/08/2014 22:55									*
 *																				*
 *		By Tuetuopay - http://tuetuopay.fr										*
 *																				*
 *		This file is part of PuShot.											*
 *																				*
 *		PuShot is free software: you can redistribute it and/or modify			*
 *		it under the terms of the GNU General Public License as published by	*
 *		the Free Software Foundation, either version 3 of the License, or		*
 *		(at your option) any later version.										*
 *																				*
 *		PuShot is distributed in the hope that it will be useful,				*
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *		GNU General Public License for more details.							*
 *																				*
 *		You should have received a copy of the GNU General Public License		*
 *		along with PuShot.  If not, see <http://www.gnu.org/licenses/>.			*
 *																				*
 ********************************************************************************/

include_once 'PS_Settings.php';

/* Opening the database */
try {
	$bdd = new PDO('mysql:host='$sqlHost.';dbname='.$sqlDB, $sqlUser, $sqlPass);
}	catch(Exception $e) {
	die('Erreur : '.$e->getMessage());
}

/* You get the point ... */
if ($logErrorsToFile)	{
	$f = fopen ($logFile, 'a');
	$IP = $_SERVER['REMOTE_ADDR'];
	$time = date ('d-m-Y H:i:s');
	$logPrefix = '['.$date.' '.$IP.'] ';
}

function errorAndExit ($message)	{
	global $logErrorsToFile;
	echo $message;
	if ($logErrorsToFile)	{	// If we have to log errors to some file ...
		global $f;
		global $logPrefix;
		fputs ($f, $logPrefix.$message);	// We log them to the file !
		fclose ($f);
	}
	exit;
}
function protect ($string)	{
	return hash('sha512', $string);
}
function hashPassword ($username, $password)	{
	$username = strtolower($username);

	for ($i = 0; $i < 1000; $i ++)	{	// You may want to increase the 1000 here ... may take longer to compute though ...
		$password = protect ($username) . protect ($password . $username);
	}

	return $password;
}
function canUserLogin ($bdd, $username, $password)	{
	global $sqlUsersTable;
	$req = $bdd->prepare ('SELECT `hashedPassword` FROM `'.$sqlUsersTable.'` WHERE UPPER(`username`) = UPPER(:username)');
	$req->execute (array ('username' => $username));
	$data = $req->fetch ();
	$req->closeCursor ();

	return !($data == null || hashPassword ($username, $password) != $data['hashedPassword']);
}

if ($needsLogin)	{ /* Let's log in the user */
	if (isset ($_POST['login']) && isset ($_POST['password']))	{
		if (!empty ($_POST['login']) && !empty ($_POST['password']))	{
			$req = $bdd->prepare ('SELECT `session_key`, `session_time` FROM `'.$sqlUsersTable.'` WHERE UPPER(`username`) = UPPER(:username)');
			$req->execute (arary ('username' => $_POST['login']));

			$now = time();
		}
		else
			errorAndExit ('Authentication failed : login or password empty.');
	}
	else
		errorAndExit ('Authentication failed : no login or password sent.');
}

/* Checking if the file was correctly uploaded */
if (!isset ($_FILES['media']) OR $_FILES['media']['error'] != 0)	{
	echo 'File not uploaded. Code = '.$_FILES['media']['error'];
	if ($logErrorsToFile)	{
		fputs ($f, "We got an error ! : ".'{"error":{"message":"File not uploaded","code":'.$_FILES['media']['error'].'}}'."\n");
		fclose ($f);
	}
	exit;
}
/* Checking file extension (ya know, I don't want to get a PHP file in here ...) */
$ext = strtolower (pathinfo ($_FILES['media']['name'])['extension']);

/* Inserting all the data in an entry with a (hopefully) unique token. */
$req = $bdd->prepare ('INSERT INTO `'.$sqlMainTable.'`(`user`, `date`, `size`) VALUES (:user, NOW(), :size)');
$req->execute (array ('user' => $_POST['login'], 'size' => $_FILES['media']['size']));
$id = $bdd->lastInsertId ('id');
$req->closeCursor ();

move_uploaded_file ($_FILES['media']['tmp_name'], $storageFolder.dechex ($id).'.'.$ext);

echo 'http://'.$baseURL.'/'.dechex ($id).'.'.$ext;
if ($logErrorsToFile)
	fclose ($f);

?>