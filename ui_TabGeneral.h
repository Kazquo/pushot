/********************************************************************************
** Form generated from reading UI file 'TabGeneral.ui'
**
** Created by: Qt User Interface Compiler version 4.8.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TABGENERAL_H
#define UI_TABGENERAL_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TabGeneral
{
public:
    QVBoxLayout *verticalLayout;
    QGroupBox *gbGeneral;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *cbStartOnBoot;
    QLabel *lblScreenshotPah;
    QHBoxLayout *layScreenshotPath;
    QLineEdit *leScreenshotPath;
    QToolButton *btnBrowse;
    QGroupBox *gbOnUpload;
    QVBoxLayout *verticalLayout_3;
    QCheckBox *cbNotifVisual;
    QCheckBox *cbNotifSound;
    QCheckBox *cbCopyClipboard;
    QCheckBox *cbOpenInBrowser;
    QCheckBox *cbDelete;
    QGroupBox *gbKeyboard;
    QVBoxLayout *verticalLayout_4;
    QCheckBox *cbSystemKeys;

    void setupUi(QWidget *TabGeneral)
    {
        if (TabGeneral->objectName().isEmpty())
            TabGeneral->setObjectName(QString::fromUtf8("TabGeneral"));
        TabGeneral->resize(400, 315);
        TabGeneral->setMinimumSize(QSize(0, 242));
        verticalLayout = new QVBoxLayout(TabGeneral);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gbGeneral = new QGroupBox(TabGeneral);
        gbGeneral->setObjectName(QString::fromUtf8("gbGeneral"));
        verticalLayout_2 = new QVBoxLayout(gbGeneral);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        cbStartOnBoot = new QCheckBox(gbGeneral);
        cbStartOnBoot->setObjectName(QString::fromUtf8("cbStartOnBoot"));
        cbStartOnBoot->setChecked(true);

        verticalLayout_2->addWidget(cbStartOnBoot);

        lblScreenshotPah = new QLabel(gbGeneral);
        lblScreenshotPah->setObjectName(QString::fromUtf8("lblScreenshotPah"));

        verticalLayout_2->addWidget(lblScreenshotPah);

        layScreenshotPath = new QHBoxLayout();
        layScreenshotPath->setObjectName(QString::fromUtf8("layScreenshotPath"));
        leScreenshotPath = new QLineEdit(gbGeneral);
        leScreenshotPath->setObjectName(QString::fromUtf8("leScreenshotPath"));

        layScreenshotPath->addWidget(leScreenshotPath);

        btnBrowse = new QToolButton(gbGeneral);
        btnBrowse->setObjectName(QString::fromUtf8("btnBrowse"));

        layScreenshotPath->addWidget(btnBrowse);


        verticalLayout_2->addLayout(layScreenshotPath);


        verticalLayout->addWidget(gbGeneral);

        gbOnUpload = new QGroupBox(TabGeneral);
        gbOnUpload->setObjectName(QString::fromUtf8("gbOnUpload"));
        verticalLayout_3 = new QVBoxLayout(gbOnUpload);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        cbNotifVisual = new QCheckBox(gbOnUpload);
        cbNotifVisual->setObjectName(QString::fromUtf8("cbNotifVisual"));
        cbNotifVisual->setChecked(true);

        verticalLayout_3->addWidget(cbNotifVisual);

        cbNotifSound = new QCheckBox(gbOnUpload);
        cbNotifSound->setObjectName(QString::fromUtf8("cbNotifSound"));
        cbNotifSound->setChecked(true);

        verticalLayout_3->addWidget(cbNotifSound);

        cbCopyClipboard = new QCheckBox(gbOnUpload);
        cbCopyClipboard->setObjectName(QString::fromUtf8("cbCopyClipboard"));
        cbCopyClipboard->setChecked(true);

        verticalLayout_3->addWidget(cbCopyClipboard);

        cbOpenInBrowser = new QCheckBox(gbOnUpload);
        cbOpenInBrowser->setObjectName(QString::fromUtf8("cbOpenInBrowser"));

        verticalLayout_3->addWidget(cbOpenInBrowser);

        cbDelete = new QCheckBox(gbOnUpload);
        cbDelete->setObjectName(QString::fromUtf8("cbDelete"));

        verticalLayout_3->addWidget(cbDelete);


        verticalLayout->addWidget(gbOnUpload);

        gbKeyboard = new QGroupBox(TabGeneral);
        gbKeyboard->setObjectName(QString::fromUtf8("gbKeyboard"));
        gbKeyboard->setCheckable(false);
        verticalLayout_4 = new QVBoxLayout(gbKeyboard);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        cbSystemKeys = new QCheckBox(gbKeyboard);
        cbSystemKeys->setObjectName(QString::fromUtf8("cbSystemKeys"));
        cbSystemKeys->setChecked(true);

        verticalLayout_4->addWidget(cbSystemKeys);


        verticalLayout->addWidget(gbKeyboard);


        retranslateUi(TabGeneral);

        QMetaObject::connectSlotsByName(TabGeneral);
    } // setupUi

    void retranslateUi(QWidget *TabGeneral)
    {
        TabGeneral->setWindowTitle(QApplication::translate("TabGeneral", "Form", 0, QApplication::UnicodeUTF8));
        gbGeneral->setTitle(QApplication::translate("TabGeneral", "General", 0, QApplication::UnicodeUTF8));
        cbStartOnBoot->setText(QApplication::translate("TabGeneral", "Start on system boot", 0, QApplication::UnicodeUTF8));
        lblScreenshotPah->setText(QApplication::translate("TabGeneral", "Screenshots folder :", 0, QApplication::UnicodeUTF8));
        leScreenshotPath->setPlaceholderText(QApplication::translate("TabGeneral", "~/Desktop/", 0, QApplication::UnicodeUTF8));
        btnBrowse->setText(QApplication::translate("TabGeneral", "...", 0, QApplication::UnicodeUTF8));
        gbOnUpload->setTitle(QApplication::translate("TabGeneral", "On upload completion", 0, QApplication::UnicodeUTF8));
        cbNotifVisual->setText(QApplication::translate("TabGeneral", "Show a visual notification", 0, QApplication::UnicodeUTF8));
        cbNotifSound->setText(QApplication::translate("TabGeneral", "Play a sound", 0, QApplication::UnicodeUTF8));
        cbCopyClipboard->setText(QApplication::translate("TabGeneral", "Copy link to clipboard", 0, QApplication::UnicodeUTF8));
        cbOpenInBrowser->setText(QApplication::translate("TabGeneral", "Open in browser", 0, QApplication::UnicodeUTF8));
        cbDelete->setText(QApplication::translate("TabGeneral", "Delete from disk", 0, QApplication::UnicodeUTF8));
        gbKeyboard->setTitle(QApplication::translate("TabGeneral", "Keyboard Shortcuts", 0, QApplication::UnicodeUTF8));
        cbSystemKeys->setText(QApplication::translate("TabGeneral", "Use system shortcuts (if the system have some)", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class TabGeneral: public Ui_TabGeneral {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TABGENERAL_H
