/********************************************************************************
** Form generated from reading UI file 'TabCredentials.ui'
**
** Created by: Qt User Interface Compiler version 4.8.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TABCREDENTIALS_H
#define UI_TABCREDENTIALS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QFormLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TabCredentials
{
public:
    QVBoxLayout *verticalLayout;
    QGroupBox *gbDetails;
    QVBoxLayout *verticalLayout_2;
    QFormLayout *layCustomCredentials;
    QLabel *lblLogin;
    QLineEdit *leLogin;
    QLabel *lblPassword;
    QLineEdit *lePassword;
    QRadioButton *rbPuShotServer;
    QGroupBox *gbPSAccount;
    QFormLayout *formLayout;
    QLabel *lblAccount;
    QLabel *lblPSLogin;
    QPushButton *btnViewAccount;
    QPushButton *btnLogout;
    QRadioButton *rbCustomServer;
    QGroupBox *gbCustomServer;
    QVBoxLayout *verticalLayout_3;
    QCheckBox *cbNeedsLogin;
    QCheckBox *cbSsl;
    QHBoxLayout *layApiUrl;
    QLabel *lblApiUrl;
    QLineEdit *leApiUrl;

    void setupUi(QWidget *TabCredentials)
    {
        if (TabCredentials->objectName().isEmpty())
            TabCredentials->setObjectName(QString::fromUtf8("TabCredentials"));
        TabCredentials->resize(400, 366);
        verticalLayout = new QVBoxLayout(TabCredentials);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gbDetails = new QGroupBox(TabCredentials);
        gbDetails->setObjectName(QString::fromUtf8("gbDetails"));
        verticalLayout_2 = new QVBoxLayout(gbDetails);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        layCustomCredentials = new QFormLayout();
        layCustomCredentials->setObjectName(QString::fromUtf8("layCustomCredentials"));
        layCustomCredentials->setFieldGrowthPolicy(QFormLayout::FieldsStayAtSizeHint);
        lblLogin = new QLabel(gbDetails);
        lblLogin->setObjectName(QString::fromUtf8("lblLogin"));

        layCustomCredentials->setWidget(0, QFormLayout::LabelRole, lblLogin);

        leLogin = new QLineEdit(gbDetails);
        leLogin->setObjectName(QString::fromUtf8("leLogin"));

        layCustomCredentials->setWidget(0, QFormLayout::FieldRole, leLogin);

        lblPassword = new QLabel(gbDetails);
        lblPassword->setObjectName(QString::fromUtf8("lblPassword"));

        layCustomCredentials->setWidget(1, QFormLayout::LabelRole, lblPassword);

        lePassword = new QLineEdit(gbDetails);
        lePassword->setObjectName(QString::fromUtf8("lePassword"));
        lePassword->setEchoMode(QLineEdit::Password);

        layCustomCredentials->setWidget(1, QFormLayout::FieldRole, lePassword);


        verticalLayout_2->addLayout(layCustomCredentials);

        rbPuShotServer = new QRadioButton(gbDetails);
        rbPuShotServer->setObjectName(QString::fromUtf8("rbPuShotServer"));
        rbPuShotServer->setChecked(true);

        verticalLayout_2->addWidget(rbPuShotServer);

        gbPSAccount = new QGroupBox(gbDetails);
        gbPSAccount->setObjectName(QString::fromUtf8("gbPSAccount"));
        formLayout = new QFormLayout(gbPSAccount);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        lblAccount = new QLabel(gbPSAccount);
        lblAccount->setObjectName(QString::fromUtf8("lblAccount"));

        formLayout->setWidget(0, QFormLayout::LabelRole, lblAccount);

        lblPSLogin = new QLabel(gbPSAccount);
        lblPSLogin->setObjectName(QString::fromUtf8("lblPSLogin"));

        formLayout->setWidget(0, QFormLayout::FieldRole, lblPSLogin);

        btnViewAccount = new QPushButton(gbPSAccount);
        btnViewAccount->setObjectName(QString::fromUtf8("btnViewAccount"));

        formLayout->setWidget(1, QFormLayout::LabelRole, btnViewAccount);

        btnLogout = new QPushButton(gbPSAccount);
        btnLogout->setObjectName(QString::fromUtf8("btnLogout"));

        formLayout->setWidget(1, QFormLayout::FieldRole, btnLogout);


        verticalLayout_2->addWidget(gbPSAccount);

        rbCustomServer = new QRadioButton(gbDetails);
        rbCustomServer->setObjectName(QString::fromUtf8("rbCustomServer"));

        verticalLayout_2->addWidget(rbCustomServer);

        gbCustomServer = new QGroupBox(gbDetails);
        gbCustomServer->setObjectName(QString::fromUtf8("gbCustomServer"));
        gbCustomServer->setEnabled(false);
        verticalLayout_3 = new QVBoxLayout(gbCustomServer);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        cbNeedsLogin = new QCheckBox(gbCustomServer);
        cbNeedsLogin->setObjectName(QString::fromUtf8("cbNeedsLogin"));

        verticalLayout_3->addWidget(cbNeedsLogin);

        cbSsl = new QCheckBox(gbCustomServer);
        cbSsl->setObjectName(QString::fromUtf8("cbSsl"));
        cbSsl->setEnabled(false);

        verticalLayout_3->addWidget(cbSsl);

        layApiUrl = new QHBoxLayout();
        layApiUrl->setObjectName(QString::fromUtf8("layApiUrl"));
        lblApiUrl = new QLabel(gbCustomServer);
        lblApiUrl->setObjectName(QString::fromUtf8("lblApiUrl"));

        layApiUrl->addWidget(lblApiUrl);

        leApiUrl = new QLineEdit(gbCustomServer);
        leApiUrl->setObjectName(QString::fromUtf8("leApiUrl"));

        layApiUrl->addWidget(leApiUrl);


        verticalLayout_3->addLayout(layApiUrl);


        verticalLayout_2->addWidget(gbCustomServer);


        verticalLayout->addWidget(gbDetails);


        retranslateUi(TabCredentials);
        QObject::connect(rbPuShotServer, SIGNAL(toggled(bool)), gbPSAccount, SLOT(setEnabled(bool)));
        QObject::connect(rbCustomServer, SIGNAL(toggled(bool)), gbCustomServer, SLOT(setEnabled(bool)));
        QObject::connect(cbNeedsLogin, SIGNAL(toggled(bool)), cbSsl, SLOT(setEnabled(bool)));

        QMetaObject::connectSlotsByName(TabCredentials);
    } // setupUi

    void retranslateUi(QWidget *TabCredentials)
    {
        TabCredentials->setWindowTitle(QApplication::translate("TabCredentials", "Form", 0, QApplication::UnicodeUTF8));
        gbDetails->setTitle(QApplication::translate("TabCredentials", "Details", 0, QApplication::UnicodeUTF8));
        lblLogin->setText(QApplication::translate("TabCredentials", "Login", 0, QApplication::UnicodeUTF8));
        leLogin->setPlaceholderText(QApplication::translate("TabCredentials", "Account Login", 0, QApplication::UnicodeUTF8));
        lblPassword->setText(QApplication::translate("TabCredentials", "Password", 0, QApplication::UnicodeUTF8));
        lePassword->setPlaceholderText(QApplication::translate("TabCredentials", "Account Password", 0, QApplication::UnicodeUTF8));
        rbPuShotServer->setText(QApplication::translate("TabCredentials", "Use PuShot servers", 0, QApplication::UnicodeUTF8));
        gbPSAccount->setTitle(QApplication::translate("TabCredentials", "PuShot Account", 0, QApplication::UnicodeUTF8));
        lblAccount->setText(QApplication::translate("TabCredentials", "Account", 0, QApplication::UnicodeUTF8));
        lblPSLogin->setText(QApplication::translate("TabCredentials", "account_login", 0, QApplication::UnicodeUTF8));
        btnViewAccount->setText(QApplication::translate("TabCredentials", "View Account", 0, QApplication::UnicodeUTF8));
        btnLogout->setText(QApplication::translate("TabCredentials", "Logout", 0, QApplication::UnicodeUTF8));
        rbCustomServer->setText(QApplication::translate("TabCredentials", "Use custom server", 0, QApplication::UnicodeUTF8));
        gbCustomServer->setTitle(QApplication::translate("TabCredentials", "Custom Server", 0, QApplication::UnicodeUTF8));
        cbNeedsLogin->setText(QApplication::translate("TabCredentials", "Needs login", 0, QApplication::UnicodeUTF8));
        cbSsl->setText(QApplication::translate("TabCredentials", "SSL", 0, QApplication::UnicodeUTF8));
        lblApiUrl->setText(QApplication::translate("TabCredentials", "API URL", 0, QApplication::UnicodeUTF8));
        leApiUrl->setPlaceholderText(QApplication::translate("TabCredentials", "http(s)://pushot.me/...", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class TabCredentials: public Ui_TabCredentials {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TABCREDENTIALS_H
