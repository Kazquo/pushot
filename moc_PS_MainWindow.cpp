/****************************************************************************
** Meta object code from reading C++ file 'PS_MainWindow.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "PS_MainWindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PS_MainWindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_PS_MainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x05,

 // slots: signature, parameters, type, tag, flags
      29,   22,   14,   14, 0x0a,
      66,   14,   14,   14, 0x0a,
      78,   14,   14,   14, 0x0a,
     107,  102,   14,   14, 0x0a,
     144,   14,   14,   14, 0x0a,
     186,  165,   14,   14, 0x0a,
     225,  220,   14,   14, 0x0a,
     270,  102,   14,   14, 0x0a,
     316,   14,   14,   14, 0x0a,
     331,   14,   14,   14, 0x0a,
     358,  348,   14,   14, 0x0a,
     379,   14,   14,   14, 0x0a,
     393,   14,   14,   14, 0x0a,
     406,   14,   14,   14, 0x0a,
     424,   14,   14,   14, 0x0a,
     445,   14,   14,   14, 0x0a,
     464,   14,   14,   14, 0x0a,
     487,   14,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_PS_MainWindow[] = {
    "PS_MainWindow\0\0quit()\0action\0"
    "on_toolBar_actionTriggered(QAction*)\0"
    "atAppExit()\0on_animation_finished()\0"
    "path\0on_watcher_directoryChanged(QString)\0"
    "on_upload_finished()\0bytesSent,bytesTotal\0"
    "on_upload_progress(qint64,qint64)\0"
    "code\0on_upload_error(QNetworkReply::NetworkError)\0"
    "on_tabGeneral_screenshotsPathChanged(QString)\0"
    "shootDesktop()\0shootSelection()\0"
    "selection\0selectionMade(QRect)\0"
    "shootWindow()\0uploadFile()\0uploadClipboard()\0"
    "uploadClipboardExt()\0disableRequested()\0"
    "preferencesRequested()\0quitRequested()\0"
};

void PS_MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        PS_MainWindow *_t = static_cast<PS_MainWindow *>(_o);
        switch (_id) {
        case 0: _t->quit(); break;
        case 1: _t->on_toolBar_actionTriggered((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 2: _t->atAppExit(); break;
        case 3: _t->on_animation_finished(); break;
        case 4: _t->on_watcher_directoryChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->on_upload_finished(); break;
        case 6: _t->on_upload_progress((*reinterpret_cast< qint64(*)>(_a[1])),(*reinterpret_cast< qint64(*)>(_a[2]))); break;
        case 7: _t->on_upload_error((*reinterpret_cast< QNetworkReply::NetworkError(*)>(_a[1]))); break;
        case 8: _t->on_tabGeneral_screenshotsPathChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 9: _t->shootDesktop(); break;
        case 10: _t->shootSelection(); break;
        case 11: _t->selectionMade((*reinterpret_cast< QRect(*)>(_a[1]))); break;
        case 12: _t->shootWindow(); break;
        case 13: _t->uploadFile(); break;
        case 14: _t->uploadClipboard(); break;
        case 15: _t->uploadClipboardExt(); break;
        case 16: _t->disableRequested(); break;
        case 17: _t->preferencesRequested(); break;
        case 18: _t->quitRequested(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData PS_MainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject PS_MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_PS_MainWindow,
      qt_meta_data_PS_MainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &PS_MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *PS_MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *PS_MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PS_MainWindow))
        return static_cast<void*>(const_cast< PS_MainWindow*>(this));
    if (!strcmp(_clname, "Ui_MainWindow"))
        return static_cast< Ui_MainWindow*>(const_cast< PS_MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int PS_MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    }
    return _id;
}

// SIGNAL 0
void PS_MainWindow::quit()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
