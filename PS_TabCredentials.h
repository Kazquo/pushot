/********************************************************************************
 *																				*
 *		PS_TabCredentials.h - Created 25/08/2014 01:20							*
 *																				*
 *		By Tuetuopay - http://tuetuopay.fr										*
 *																				*
 *		This file is part of PuShot.											*
 *																				*
 *		PuShot is free software: you can redistribute it and/or modify			*
 *		it under the terms of the GNU General Public License as published by	*
 *		the Free Software Foundation, either version 3 of the License, or		*
 *		(at your option) any later version.										*
 *																				*
 *		PuShot is distributed in the hope that it will be useful,				*
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *		GNU General Public License for more details.							*
 *																				*
 *		You should have received a copy of the GNU General Public License		*
 *		along with PuShot.  If not, see <http://www.gnu.org/licenses/>.			*
 *																				*
 ********************************************************************************/

#ifndef _PS_TAB_CREDENTIALS_H
#define _PS_TAB_CREDENTIALS_H

#include <QtGui>

#include "ui_TabCredentials.h"

#include "PS_Tab.h"

class PS_TabCredentials : public PS_Tab, protected Ui_TabCredentials	{
public:
	PS_TabCredentials (QWidget *parent = NULL);
	~PS_TabCredentials ();
	
	QString apiUrl ();
	QString username ();
	QString password ();
	bool useAuth ();
	
	void saveSettings (QSettings &s);
	void loadSettings (QSettings &s);
};

#endif /* defined (_PS_TAB_CREDENTIALS_H) */
